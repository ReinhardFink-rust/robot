// define size of robots room
pub const COLUMNS : usize = 20;
pub const ROWS : usize = 6;

// define chars for a basic empty field
pub const FIELD_FREE : char = '.';
pub const FIELD_WALL : char = '#';

// define chars for four robot direction
pub const ROBOT_UP: char = '^';
pub const ROBOT_LEFT: char = '<';
pub const ROBOT_DOWN: char = 'v';
pub const ROBOT_RIGHT: char = '>';

// define sleep time between picture printing
pub const SLEEP: u64 = 500;



