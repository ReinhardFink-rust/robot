use crate::constants::*;
use crate::location::Location;

pub struct World {
    grid: [[char; COLUMNS]; ROWS]
}

impl World {

    pub fn new() -> World {
        let mut world = World {
            grid: [[FIELD_FREE; COLUMNS]; ROWS]
        };
        world.grid[0] = [FIELD_WALL; COLUMNS];
        world.grid[ROWS - 1] = [FIELD_WALL; COLUMNS];
        for row in world.grid.iter_mut() {
            row[0] = FIELD_WALL;
            row[COLUMNS - 1] = FIELD_WALL;
        }
        return world;
    }

    pub fn print(&self) {
        for row in self.grid.iter() {
            //println!("row is {:?}", row)
            for c in row.iter() {
                print!("{}", c);
            }
            println!();
        }
    }

    pub fn get_char_at(&self, loc: Location) -> char {
        self.grid[loc.row][loc.col]
    }

    pub fn set_char_at(&mut self, loc: Location, c: char) {
        self.grid[loc.row][loc.col] = c;
    }
}
