mod location;
mod world;
mod robot;
mod robot_mt_01;
mod constants;

use crate::constants::*;
use crate::location::Location;
use crate::world::World;
use crate::robot::Robot;
use crate::robot_mt_01::RobotMT01;
use std::sync::{Mutex, Arc};
use std::thread;

fn main() {
    //single_thread();
    multi_thread_01();
    //multi_thread_02();
}

fn single_thread() {
    println!("START SINGLE THREAD");
    let mut world = World::new();
    world.set_char_at(Location::new(2, 5), FIELD_WALL);
    world.set_char_at(Location::new(3, 7), FIELD_WALL);
    world.print();
    let loc = Location { row: 1, col: 1 };
    let mut robot_1 = Robot::new(&mut world, loc, ROBOT_RIGHT);
    robot_1.turn_right();
    robot_1.turn_left();
    let loc = Location { row: 3, col: 1 };
    let mut robot_2 = Robot::new(&mut world, loc, ROBOT_RIGHT);
    while robot_2.free() {
        robot_2.mv();
    }
    //let mut robot_3 = Robot::new(&mut world, Location::new(4, 0), ROBOT_LEFT);
    //robot_3.look();
    //robot_3.mv();
}

fn multi_thread_01() {
    println!("START MULTI THREAD 01");
    let mut world = World::new();
    world.set_char_at(Location::new(2, 5), FIELD_WALL);
    world.set_char_at(Location::new(3, 17), FIELD_WALL);
    world.print();
    let world = Arc::new(Mutex::new(world));
    let w = world.clone();
    let child = thread::spawn(move || {
        let loc = Location { row: 1, col: 1 };
        let mut robot_1 = RobotMT01::new(w, loc, ROBOT_RIGHT);
        robot_1.mv();
        robot_1.turn_right();
        robot_1.turn_left();
        while robot_1.free() {
            robot_1.mv();
        }
    });

    let loc = Location { row: 3, col: 1 };
    let mut robot_2 = RobotMT01::new(world.clone(), loc, ROBOT_RIGHT);
    while robot_2.free() {
        robot_2.mv();
    }
    robot_2.turn_left();
    robot_2.turn_left();
    while robot_2.free() {
        robot_2.mv();
    }
    //let mut robot_3 = RobotMT01::new(world.clone(), Location::new(4, 0), ROBOT_LEFT);
    //robot_3.look();
    //robot_3.mv();
    let _res = child.join();
}

fn multi_thread_02() {
    println!("START MULTI THREAD 02");
    let world = World::new();
    let world = Arc::new(Mutex::new(world));
    let mut handles = Vec::with_capacity((ROWS - 2)*2);
    for row in 1..ROWS - 1 {
        let world = world.clone();
        let hl = thread::spawn(move || {
            let loc = Location::new(row, 1);
            let mut robot = RobotMT01::new(world, loc, ROBOT_RIGHT);
            while robot.free() {
                robot.mv();
            }
        });
        handles.push(hl);
    }
    for row in 1..ROWS - 1 {
        let world = world.clone();
        let hr = thread::spawn(move || {
            let loc = Location::new(row, COLUMNS - 2);
            let mut robot = RobotMT01::new(world, loc, ROBOT_LEFT);
            while robot.free() {
                robot.mv();
            }
        });
        handles.push(hr);
    }

    for h in handles.into_iter() {
        //h.join(); <- this `Result` may be an `Err` variant, which should be handled
        /*
        match h.join() {
            Ok(x) => x,
            Err(_) => todo!(),
        };

         */
        h.join().expect("Error joining Handle");
    }
    /* short version
    for h in handles {
        h.join().unwrap();
    }
     */
}