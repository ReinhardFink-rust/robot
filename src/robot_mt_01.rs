use crate::constants::*;
use crate::location::Location;
use crate::world::World;
use std::{thread, time};
use std::ops::{Deref, DerefMut};
use std::sync::{Arc, Mutex};

pub struct RobotMT01 {
    world: Arc<Mutex<World>>,
    loc: Location,
    direction: char,
}

impl RobotMT01 {
    pub fn new(world: Arc<Mutex<World>>, loc: Location, direction: char) -> RobotMT01 {
        let robot = RobotMT01 {
            world: world.clone(),
            loc,
            direction,
        };
        robot.world.lock().unwrap().set_char_at(loc, direction);
        robot.update_graphic();
        robot
    }

    fn get_location_in_front(&self) -> Location {
        //let mut np : Location = { row : self.loc.row, col : self.loc.col };
        let mut location_in_front = self.loc;
        match self.direction {
            //ROBOT_UP => if self.loc.row > 0 { location_in_front.row = self.loc.row - 1 },
            //ROBOT_LEFT => if self.loc.col > 0 { location_in_front.col = self.loc.col - 1 },
            ROBOT_UP => location_in_front.row = self.loc.row - 1,
            ROBOT_LEFT => location_in_front.col = self.loc.col - 1,
            ROBOT_DOWN => location_in_front.row = self.loc.row + 1,
            ROBOT_RIGHT => location_in_front.col = self.loc.col + 1,
            _ => println!("wrong char for direction")
        }
        location_in_front
    }

    pub fn look(&self) -> char {
        return self.world.lock().unwrap().get_char_at(self.get_location_in_front());
    }

    pub fn free(&self) -> bool {
        self.look() == FIELD_FREE
    }

    pub fn mv(&mut self) {
        let new_loc = self.get_location_in_front();
        if self.free() {
            // TODO: free field may be set to NOT free by an other thread just here!
            {   // block to release lock on world
                // before self.update_graphic() with his own lock is called
                // world1 does NOT block
                // world2 blocks
                //let mut world1 = self.world.lock().unwrap().set_char_at(self.loc, FIELD_FREE);
                let mut world = self.world.lock().unwrap();
                //let mut world2 = self.world.lock().unwrap().set_char_at(self.loc, FIELD_FREE);
                world.set_char_at(self.loc, FIELD_FREE);
                self.loc = new_loc;
                world.set_char_at(self.loc, self.direction);
                /* next 2 lines will produce a deadlock self.update_graphics, will wait to get lock
                println!("++++++++++++");
                self.update_graphic();
                 */
            }
            self.update_graphic();
        } else {
            println!("Autsch");
        }
    }


    pub fn turn_left(&mut self) {
        match self.direction {
            ROBOT_UP => self.direction = ROBOT_LEFT,
            ROBOT_LEFT => self.direction = ROBOT_DOWN,
            ROBOT_DOWN => self.direction = ROBOT_RIGHT,
            ROBOT_RIGHT => self.direction = ROBOT_UP,
            _ => println!("wrong char for direction")
        }
        {
            self.world.lock().unwrap().set_char_at(self.loc, self.direction);
        }
        self.update_graphic();
    }

    pub fn turn_right(&mut self) {
        match self.direction {
            ROBOT_UP => self.direction = ROBOT_RIGHT,
            ROBOT_LEFT => self.direction = ROBOT_UP,
            ROBOT_DOWN => self.direction = ROBOT_LEFT,
            ROBOT_RIGHT => self.direction = ROBOT_DOWN,
            _ => println!("wrong char for direction")
        }
        self.world.lock().unwrap().set_char_at(self.loc, self.direction);
        self.update_graphic();
    }

    pub fn update_graphic(&self) {
        //println!("**********");
        print!("\x1B[2J"); // clear screen
        //let mut world = self.world.lock().unwrap();
        //world.print();
        // the fast version, uses deref()
        self.world.lock().unwrap().print();
        thread::sleep(time::Duration::from_millis(SLEEP));
    }
}


