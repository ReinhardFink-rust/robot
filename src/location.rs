#[derive(Copy, Clone)]
pub struct Location {
    pub row: usize,
    pub col: usize,
}

impl Location {

    pub fn new(row: usize, col: usize) -> Location {
        Location { row, col }
        //let new_loc = Location { row, col };
        //new_loc
    }
}