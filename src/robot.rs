use crate::constants::*;
use crate::location::Location;
use crate::world::World;
use std::{thread, time};

pub struct Robot<'a> {
    world: &'a mut World,
    loc: Location,
    direction: char,
}

impl<'a> Robot<'a> {
    pub fn new(world: &'a mut World, loc: Location, direction: char) -> Robot<'a> {
        world.set_char_at(loc, direction);
        world.print();
        Robot {
            world,
            loc,
            direction,
        }
    }

    fn get_location_in_front(&self) -> Location {
        //let mut np : Location = { row : self.loc.row, col : self.loc.col };
        let mut location_in_front = self.loc;
        match self.direction {
            //ROBOT_UP => if self.loc.row > 0 { location_in_front.row = self.loc.row - 1 },
            //ROBOT_LEFT => if self.loc.col > 0 { location_in_front.col = self.loc.col - 1 },
            ROBOT_UP => location_in_front.row = self.loc.row - 1,
            ROBOT_LEFT => location_in_front.col = self.loc.col - 1,
            ROBOT_DOWN => location_in_front.row = self.loc.row + 1,
            ROBOT_RIGHT => location_in_front.col = self.loc.col + 1,
            _ => println!("wrong char for direction")
        }
        location_in_front
    }

    pub fn look(&self) -> char {
        self.world.get_char_at(self.get_location_in_front())
    }

    pub fn free(&self) -> bool {
        self.look() == FIELD_FREE
    }

    pub fn mv(&mut self) {
        let new_loc = self.get_location_in_front();
        if self.free() {
            self.world.set_char_at(self.loc, FIELD_FREE);
            self.loc = new_loc;
            self.world.set_char_at(self.loc, self.direction);
            self.update_graphic();
        } else {
            println!("Autsch");
        }
    }

    pub fn turn_left(&mut self) {
        match self.direction {
            ROBOT_UP => self.direction = ROBOT_LEFT,
            ROBOT_LEFT => self.direction = ROBOT_DOWN,
            ROBOT_DOWN => self.direction = ROBOT_RIGHT,
            ROBOT_RIGHT => self.direction = ROBOT_UP,
            _ => println!("wrong char for direction")
        }
        self.world.set_char_at(self.loc, self.direction);
        self.update_graphic();
    }

    pub fn turn_right(&mut self) {
        match self.direction {
            ROBOT_UP => self.direction = ROBOT_RIGHT,
            ROBOT_LEFT => self.direction = ROBOT_UP,
            ROBOT_DOWN => self.direction = ROBOT_LEFT,
            ROBOT_RIGHT => self.direction = ROBOT_DOWN,
            _ => println!("wrong char for direction")
        }
        self.world.set_char_at(self.loc, self.direction);
        self.update_graphic();
    }

    fn update_graphic(&self) {
        print!("\x1B[2J"); // clear screen
        self.world.print();
        thread::sleep(time::Duration::from_millis(SLEEP));
    }
}


